"  __   __   ________  ___ __ __   ______    ______      
" /_/\ /_/\ /_______/\/__//_//_/\ /_____/\  /_____/\     
" \:\ \\ \ \\__.::._\/\::\| \| \ \\:::_ \ \ \:::__\/     
"  \:\ \\ \ \  \::\ \  \:.      \ \\:(_) ) )_\:\ \  __   
"   \:\_/.:\ \ _\::\ \__\:.\-/\  \ \\: __ `\ \\:\ \/_/\  
"    \ ..::/ //__\::\__/\\. \  \  \ \\ \ `\ \ \\:\_\ \ \ 
"     \___/_( \________\/ \__\/ \__\/ \_\/ \_\/ \_____\/ 
"    					- sm0g's vimrc

" Vundle Related Things
set nocompatible              " be iMproved, required
filetype off                  " required

" Plug.vim Things
call plug#begin('~/.vim/plugged')
" Plugins
Plug 'junegunn/goyo.vim'
Plug 'git@github.com:vim-syntastic/syntastic.git'
Plug 'nathanaelkane/vim-indent-guides'
Plug 'tpope/vim-surround'
Plug 'SirVer/ultisnips'
Plug 'shougo/neocomplete.vim'
Plug 'git@github.com:amix/vim-zenroom2.git'
Plug 'ervandew/supertab'
Plug 'scrooloose/nerdtree'
Plug 'easymotion/vim-easymotion'
Plug 'itchyny/lightline.vim'
Plug 'nerdypepper/agila.vim'
Plug 'reedes/vim-colors-pencil'

"Deoplete
if has('nvim')
  Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
else
  Plug 'Shougo/deoplete.nvim'
  Plug 'roxma/nvim-yarp'
  Plug 'roxma/vim-hug-neovim-rpc'
endif
let g:deoplete#enable_at_startup = 1

call plug#end()

" Make VIM look pretty
syntax on
set nu
set cursorline
hi CursorLine term=bold cterm=bold guibg=Grey40
hi LineNr term=bold cterm=NONE ctermfg=DarkGrey ctermbg=NONE gui=NONE guifg=DarkGrey guibg=NONE
set laststatus=2
color badwolf
let g:indent_guides_enable_on_vim_startup = 1

" Remap <leader> to ,
let mapleader=","

" Easily get into zenroom
nnoremap <silent> <leader>z :Goyo<cr>

" Badwolf Mods
let g:badwolf_darkgutter = 1
let g:badwolf_tabline = 0


" gVIM font
if has("gui_running")
  if has("gui_gtk2")
    set guifont=Liberation\ Mono:17
  elseif has("gui_macvim")
    set guifont=Menlo\ Regular:h14
  elseif has("gui_win32")
    set guifont=Consolas:h11:cANSI
  endif
endif
